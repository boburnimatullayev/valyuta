export const allTotalPrice = (arr,type, sold) => {

     if(arr?.length > 0){
        return   arr?.map((item) => {
            if (item?.money_type  === type) {
              if (item?.took_sold === sold) {
                return +item?.money_total
              }else{
                return 0
              }
            }
          })?.reduce((a, b) => a + b);
     }
};



export const allTotalPriceAll = (arr,type, sold) => {

  if(arr?.length > 0){
     return   arr?.map((item) => {
         if (item?.money_type  === type) {
           if (item?.took_sold === sold) {
             return +item?.money_number
           }else{
             return 0
           }
         }
       })?.reduce((a, b) => a + b);
  }
};







