export const numberToPrice = (number) => {

    return `${Math.trunc(number)?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')} `
  }
  