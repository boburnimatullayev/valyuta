
import axios from 'axios';
import { notification } from 'antd';
import authStore from '../store/auth.store';

const unauthorizedCode = [400,403]

const request = axios.create({
  baseURL: 'https://api.valyota.uz/',
  timeout: 60000
})

// Config
const TOKEN_PAYLOAD_KEY = 'token'

// API Request interceptor
request.interceptors.request.use(config => {
	const jwtToken = authStore?.token;
	
	if (jwtToken) {
		config.headers[TOKEN_PAYLOAD_KEY] = jwtToken
	}

  	return config
}, error => {
	// Do something with request error here
	notification.error({
		message: 'Error'
	})
	Promise.reject(error)
})

// API respone interceptor
request.interceptors.response.use( (response) => {
	return response.data
}, (error) => {

	let notificationParam = {
		message: ''
	}

	// Remove token and redirect 
	if (unauthorizedCode.includes(error.response.status)) {
		notificationParam.message = 'Authentication Fail'
		notificationParam.description = 'Please login again'
		authStore.logout()
		window.location.reload()
	}

	if (error.response.status === 404) {
		notificationParam.message = 'Not Found'
	}

	if (error.response.status === 500) {
		notificationParam.message = 'Internal Server Error'
	}
	
	if (error.response.status === 508) {
		notificationParam.message = 'Time Out'
	}

	notification.error(notificationParam)

	return Promise.reject(error);
});

export default request