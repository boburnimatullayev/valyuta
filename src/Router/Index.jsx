import React, { useEffect } from "react";
import { Route, Routes, useSearchParams } from "react-router-dom";
import Mainlayout from "../Mainlayout";
import Login from "../views/Loign/Index";
import Valyuta from "../views/Valyuta";
import authStore from "../store/auth.store";

const Router = () => {
  const { isAuth } = authStore
  const [searchParams] = useSearchParams()

  useEffect(() => {
    const token = searchParams.get('token')
    if (token) {
      authStore.login({
        isAuth: true,
        token
      })
    }
  }, [])
  

  if(isAuth === true) {
    return (
     <Routes>
         <Route path="/" element={<Mainlayout />}>
          <Route index element={<Valyuta />} />
        </Route>
     </Routes>
    );
  } else {
    return (
      <Routes>
        <Route path="/" element={<Login />} />
      </Routes>
    );
  
  }
};

export default Router;
