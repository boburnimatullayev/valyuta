import { unstable_HistoryRouter as HistoryRouter } from "react-router-dom";
import Router from "./Router/Index";
import { createBrowserHistory } from "history";
console.log(<Router />)

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <HistoryRouter history={createBrowserHistory({ window })}>
          <Router />
        </HistoryRouter>
      </header>
    </div>
  );
}

export default App;
