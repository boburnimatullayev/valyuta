import { DatePicker, Table, Tabs, Tag } from "antd";
import React, { useState } from "react";
import { numberToPrice } from "../../utils/numberToprice";
import { allTotalPrice, allTotalPriceAll } from "../../utils/allTotal";
import moment from "moment/moment";

const ValyutaTable = ({ dataRubl, dataDollar,setSelectedDate,loding,setLoading }) => {
  
  console.log(dataRubl)

  const [tabIndex, setTabIndex] = useState(1);

  const { RangePicker } = DatePicker;
  const onChange = (key) => {
    setTabIndex(key);
  };
  const handleDateChange = (dates, dateStrings) => {
    setSelectedDate(dateStrings);
    setLoading(true)
  };
  const columns = [
    {
      title: "Valyuta",
      dataIndex: "money_number",
      key: "money_number",
      render: (text, data) => (
        <p>
          {numberToPrice(text)} {data?.money_type === "rubl" ? "₽" : "$"}
        </p>
      ),
    },
    {
      title: "Qanchadan olindi yoki sotild",
      dataIndex: "money_sum",
      key: "money_sum",
      render: (text) => <p>{numberToPrice(text)} sum</p>,
    },
    {
      title: "Sumda qancha bo'ldi",
      dataIndex: "money_total",
      key: "money_total",
      render: (text) => <p>{numberToPrice(text)} sum</p>,
    },
    {
      title: "Vaqti",
      dataIndex: "createdAt",
      key: "createdAt",
      render: (text) => <p>{moment(text).format(`lll`)}</p>,
    },
    {
      title: "Status",
      dataIndex: "took_sold",
      key: "took_sold",
      render: (text) =>
        text === "olindi" ? (
          <Tag style={{ fontSize: "16px" }} color="green">
            Olindi
          </Tag>
        ) : (
          <Tag style={{ fontSize: "16px" }} color="geekblue">
            Sotildi
          </Tag>
        ),
    },
    {
      title: "Xaridor turi",
      dataIndex: "money_client",
      key: "money_client",
      render: (text) =>
        text === "Online" ? (
          <Tag style={{ fontSize: "16px" }} color="green">
            Online
          </Tag>
        ) : (
          <Tag style={{ fontSize: "16px" }} color="red">
            Naqt
          </Tag>
        ),
    },
  ];

  const items = [
    {
      key: "1",
      label: <p>Rubl</p>,
      children: (
        <>
          <Table
            columns={columns}
           
            loading={loding}
            dataSource={dataRubl}
            pagination={{ pageSize: 6 }}
          />
          <h3>
            Olingan summa:  {numberToPrice(allTotalPrice(dataRubl, "rubl", "olindi") || 0)}sum
          </h3>
          <h3>
           
            Sotilgan summa: {numberToPrice(allTotalPrice(dataRubl, "rubl", "sotildi") || 0)}sum
          </h3>
          <h3>
            Foyda:  {numberToPrice(
              ((allTotalPrice(dataRubl, "rubl", "sotildi") || 0) - (allTotalPrice(dataRubl, "rubl", "olindi") || 0)) > 0 ? (allTotalPrice(dataRubl, "rubl", "sotildi") || 0) - (allTotalPrice(dataRubl, "rubl", "olindi") || 0) : 0
            )}
            sum
          </h3>
          <h3>
            Qoldiq:  {numberToPrice(
              (allTotalPriceAll(dataRubl, "rubl", "olindi") || 0) - (allTotalPriceAll(dataRubl, "rubl", "sotildi") || 0) >0 ?(allTotalPriceAll(dataRubl, "rubl", "olindi") || 0) - (allTotalPriceAll(dataRubl, "rubl", "sotildi") || 0) : 0
            )}
            ₽
          </h3> 
         

        </>
      ),
    },
    {
      key: "2",
      label: <p>Dollar</p>,
      children: (
        <>
          <Table loading={loding} columns={columns} dataSource={dataDollar} />
          <h3>
          
            Olingan summa: {numberToPrice(allTotalPrice(dataDollar, "dollar", "olindi") || 0)}sum
          </h3>
          <h3>
           
            Sotilgan summa:  {numberToPrice(allTotalPrice(dataDollar, "dollar", "sotildi") || 0)}
            sum
          </h3>
          <h3>
            Foyda:  {numberToPrice(
              ((allTotalPrice(dataRubl, "dollar", "sotildi") || 0) - (allTotalPrice(dataRubl, "dollar", "olindi") || 0)) > 0 ? (allTotalPrice(dataRubl, "dollar", "sotildi") || 0) - (allTotalPrice(dataRubl, "dollar", "olindi") || 0) : 0
            )}
            sum
          </h3>
          <h3>
            Qoldiq: {numberToPrice(
              ((allTotalPriceAll(dataDollar, "dollar", "olindi") || 0) - (allTotalPriceAll(dataDollar, "dollar", "sotildi") || 0)) > 0 ? (allTotalPriceAll(dataDollar, "dollar", "olindi") || 0) - (allTotalPriceAll(dataDollar, "dollar", "sotildi") || 0) :0
            )}
            $
          </h3>
        </>
      ),
    },
  ];
  return (
    <div style={{marginTop: "20px"}}>
      <RangePicker
        format="YYYY-MM-DD"
        onChange={handleDateChange}
  
      />
      <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
    </div>
  );
};

export default ValyutaTable;
