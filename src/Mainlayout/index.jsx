import React, { useState } from 'react';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  MoneyCollectOutlined ,
  VideoCameraOutlined,
} from '@ant-design/icons';
import { Layout, Menu, Button, theme } from 'antd';
import csl from "./MainLayout.module.scss";
import { Link, Outlet, useNavigate } from "react-router-dom";
import authStore from '../store/auth.store';
const { Header, Sider, Content } = Layout;



const Mainlayout = () => {
  const navigate = useNavigate()

  const [collapsed, setCollapsed] = useState(false);
  const logOut = () =>{
    authStore.logout()
    window.location.reload()
  }
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken()

  return (
    <div className={csl.mainlayoutCon}>
     <Layout>
      <Sider className={csl.sidebar} style={{height:'100vh'}} trigger={null} collapsible collapsed={collapsed}>
        <div style={{color:'#fff',padding:"20px",fontSize:'24px',fontWeight:`bold`}} className="logo-vertical"> 
            Valyuta
        </div>
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={['1']}
          items={[
            {
              key: '1',
              icon: <MoneyCollectOutlined />,
              label: <Link to={`/`}>
                Valuta
              </Link>,
            },

          ]}
        />
      </Sider>
      <Layout>
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
            display:'flex',
            justifyContent:'space-between',
            alignItems:'center',
            paddingRight:'20px'
          }}
        >
          <Button
            type="text"
            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: '16px',
              width: 64,
              height: 64,
            }}
          />

          <Button danger onClick={() => logOut()}>
              Chiqish
          </Button>
        </Header>
        <Content
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          <Outlet />
        </Content>
      </Layout>
    </Layout>
    </div>
  );
};

export default Mainlayout;
