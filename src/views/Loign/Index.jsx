import React from "react";
import cls from "./Login.module.scss";
import { Card, notification } from "antd";
import { Button, Checkbox, Form, Input } from "antd";
import authStore from "../../store/auth.store";
import request from "../../axios/auth";

const Login = () => {
  // const username = '!n73rC0nn3C7m3h0u7r4630u5p4r0dY';
  // const password = '7r!umph4n71Y64hun3xp3C73d1Y47hw4r7p4r0dY';
  const onFinish = (values) => {

  request({
    url:"/admin/login",
    method: "POST",
    data:{
  ...values
    },
  }).then(res =>{
    console.log('res',res)
      authStore.login(res)
      window.location.reload()
  }).catch(err =>console.log(err))
      
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div
      style={{
        width: `100%`,
        height: `100vh`,
        display: `flex`,
        justifyContent: `center`,
        alignItems: `center`,
      }}
      className={cls.loginWrap}
    >
      <Card
        title="Login"
        bordered={false}
        style={{
          width: 400,
        }}
      >
        <Form
          layout="vertical"
          name="basic"
          labelCol={{
            span: 8,
          }}
          style={{
            maxWidth: `100%`,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Login"
            name="username"
            rules={[
              {
                required: true,
                message: "Iltimos Login kiritin!",
              },
            ]}
          >
            <Input size="large" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: " Iltimos passwordni kiritin!",
              },
            ]}
          >
            <Input.Password size="large" />
          </Form.Item>

          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" size="large">
              Kiris
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
};

export default Login;
