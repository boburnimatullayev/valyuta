import React, { useEffect, useState } from "react";
import cls from "./Valyuta.module.scss";
import { Button, Flex, Form, Input, Radio, Spin } from "antd";

import ValyutaTable from "../../comonents/valyutaTable/ValyutaTable";
import moment from "moment";
import request from "../../axios/auth";



const Valyuta = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [loding,setLoading] = useState(false)
  const [selectedDate,setSelectedDate] = useState(['',''])
  const today = new Date().toISOString().slice(0, 10);
console.log(`loding`,data);

  useEffect(() => {
  
    request({
      url:'/moneys/search',
      method:'POST',
      data:{
        from_time:selectedDate[0].length > 0 ? selectedDate[0] : today,
        to_time:selectedDate[1].length > 0 ? selectedDate[1] : today
      }
    }).then(res => {
      console.log(`res`,res)
      setData(res?.reverse())
      setLoading(false)
   
    })
  }, [selectedDate[1],loding]);

  const onFinish = (values) => {  
    form.resetFields();
  request({
    url:'money/create',
    method:'POST',
    data:{
      ...values,
      money_date: moment().format("YYYY-MM-DD"),
      money_total: values.money_number * values.money_sum + ''
    }
  }).then(res =>{
      setLoading(true)
  }).catch(err => console.log(err))

    // setData([
    //   ...data,
    //   {
    //     ...values,
    //     money_date: moment().format("lll"),
    //     money_total: values.money_number * values.money_sum,
    //   },
    // ]);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className={cls.valyutaWrap}>
      <Form
        form={form}
        layout="vertical"
        name="basic"
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Flex gap={20} align="center" wrap="wrap">
          <Form.Item
            label="Valyuta"
            name="money_number"
            rules={[
              {
                required: true,
                message: "Iltimos valyuta kiriting",
              },
            ]}
          >
            <Input size="large" type="number" />
          </Form.Item>
          <Form.Item
            label="Qanchaga olindi yoki sotildi"
            name="money_sum"
            rules={[
              {
                required: true,
                message: "Iltimos sum kiriting",
              },
            ]}
          >
            <Input size="large" type="number" />
          </Form.Item>
          <div style={{ display: "flex", gap: "20px" }}>
            <Form.Item
              name="took_sold"
              rules={[
                {
                  required: true,
                  message: "Iltimos savdo turini tanlang",
                },
              ]}
            >
              <Radio.Group>
                <Radio value="olindi"> Olindi </Radio>
                <Radio value="sotildi"> Sottildi </Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              name="money_type"
              rules={[
                {
                  required: true,
                  message: "Iltimos Pul turini tanlang",
                },
              ]}
            >
              <Radio.Group>
                <Radio value="rubl"> Rubl </Radio>
                <Radio value="dollar"> Dollar </Radio>
              </Radio.Group>
            </Form.Item>
            <Form.Item
              name="money_client"
              rules={[
                {
                  required: true,
                  message: "Iltimos xaridorni tanlang",
                },
              ]}
            >
              <Radio.Group>
                <Radio value="Online"> Online </Radio>
                <Radio value="Naqt"> Naqt </Radio>
              </Radio.Group>
            </Form.Item>
          </div>
          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit" size="large">
              Qoshish
            </Button>
          </Form.Item>
        </Flex>
      </Form>

      <hr />

      <ValyutaTable
        dataRubl={data?.filter((item) => item?.money_type === "rubl") }
        dataDollar={data?.filter((item) => item?.money_type === "dollar")}
        setSelectedDate={setSelectedDate}
        loding={loding}
        setLoading={setLoading}
      />
    </div>
  );
};

export default Valyuta;
